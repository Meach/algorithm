#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define FIB_MAX 10

int fib(int n);

int main(){
    for (int i = 1; i <=FIB_MAX; i++)
    {
        printf("%d\n\r",fib(i));
    }
    system("pause");
}   

int fib(int n){
    if(n<=1) return n;
    return fib(n-1) + fib(n-2);
}